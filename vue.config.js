module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/vuetify/cm_fronted '
    : '/',
  "transpileDependencies": [
    "vuetify"
  ]
}