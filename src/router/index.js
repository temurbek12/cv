import Vue from 'vue'
import VueRouter from 'vue-router'
import Contact from "@/views/Contact";
import Portfolio from "@/views/Portfolio";

Vue.use(VueRouter)

const routes = [

  {path: '/contact',component : Contact},
  {path: '/Portfolio',component : Portfolio},
  {path: '/History',component : History}


]

const router = new VueRouter({
  routes
})

export default router
