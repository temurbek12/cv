import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router'

import Home from './views/Home';
import About from './views/About';
import Contact from './views/Contact';
import NotFound from './views/NotFound';



import Paginate from 'vuejs-paginate';



Vue.use(VueRouter)

import './scss/main.scss';
import Logout from "./views/Logout";
import Resume from "./views/Resume";

Vue.config.productionTip = false

const router = new VueRouter({
  routes: [
    { path: '/', component: Home },
    {path: '/logout', component: Logout},
    {path: '/resume', component: Resume},
    { path: '/about', component: About },
    { path: '/contact', component: Contact },
    { path: '*', component: NotFound }
  ],
  mode: 'hash'
})
Vue.component('paginate', Paginate);


new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
